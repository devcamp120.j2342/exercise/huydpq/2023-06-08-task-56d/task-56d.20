package com.devcamp.task56c10.models;

import java.util.ArrayList;

public class Country {
    private String CountryCode;
    private String CountryName;
    private ArrayList<Region> region;
    
    public Country() {
    }
    public Country(String countryCode, String countryName) {
        CountryCode = countryCode;
        CountryName = countryName;
    }
    public Country(String countryCode, String countryName, ArrayList<Region> region) {
        CountryCode = countryCode;
        CountryName = countryName;
        this.region = region;
    }
    
    public String getCountryCode() {
        return CountryCode;
    }
    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }
    public String getCountryName() {
        return CountryName;
    }
    public void setCountryName(String countryName) {
        CountryName = countryName;
    }
    public ArrayList<Region> getRegion() {
        return region;
    }
    public void setRegion(ArrayList<Region> region) {
        this.region = region;
    }

    

}
