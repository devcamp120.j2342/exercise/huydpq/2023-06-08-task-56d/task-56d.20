package com.devcamp.task56c10.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56c10.models.Country;
import com.devcamp.task56c10.service.CountryService;

@RestController
@CrossOrigin
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping("/countries/{index}")
    public Country getCountInfo(@PathVariable int index){
         

         return countryService.fillterIndex(index);
    }

    
}
